$(document).ready(function (){
    $.getJSON("https://webtechcars.herokuapp.com/api/manufacturers", function(data){
        var dropdown=$('#name');
        dropdown.append('<option selected="true" disabled>Choose Manufacturer</option>');
        dropdown.prop('selectedIndex', 0);
        $.each(data, function(key,value){
            dropdown.append($('<option></option>').text(value.name));
        })
    })
})