$(document).ready(function (){
    $.getJSON("https://webtechcars.herokuapp.com/api/cars", function(data){
        var table =$("#carTable");
        $.each(data, function(key,value){
            var row = $('<tr></tr>');
            $(table).append(row);
            var row = $('<tr></tr>');
            var idCell = $('<td>'+value._id+'</td>');
            var nameCell = $('<td>'+value.name+'</td>');
            var consumptionCell = $('<td>'+value.consumption+'</td>');
            var colorCell = $('<td>'+value.color+'</td>');
            var manufacturerCell = $('<td>'+value.manufacturer+'</td>'); 
            var availabilityCell = $('<td>'+value.avaiable+'</td>');
            var yearCell = $('<td>'+value.year+'</td>');
            var horsepowerCell = $('<td>'+value.horsepower+'</td>');           
            var updateCell = $('<td>'+"<button type='button' onclick='createDialog(this);' data-string='cars' class='btn btn-default'><img src='img/update.png'  width='50px'></button>"+'</td>');
            var deleteCell = $('<td>'+"<button type='button' onclick='deleteRecord(this);' data-string='cars' class='btn btn-default'><img src='img/delete.png'  width='50px'></button>"+'</td>');
            $(row).append(idCell);
            $(row).append(nameCell);
            $(row).append(consumptionCell);
            $(row).append(colorCell);
            $(row).append(manufacturerCell);
            $(row).append(availabilityCell);
            $(row).append(yearCell);
            $(row).append(horsepowerCell);
            $(row).append(updateCell);
            $(row).append(deleteCell);
            $(table).append();
            $(table).append(row);
        })
    })
})