function deleteRecord(that){
    var deleteID = $(that).parents("tr").children()[0].innerHTML;
    var deleteName = $(that).parents("tr").children()[1].innerHTML;
    var confirmation = confirm("Are you sure you want to delete: "+deleteName+"?");
    var dBase = $(that).data("string");

    if(confirmation){       
        $.ajax({
            url: 'https://webtechcars.herokuapp.com/api/' + dBase + '/' + deleteID,
            type: 'DELETE',
            error: function (data) {
                alert("Something went wrong...");
                console.log('Error:', data);
            },
            success: function (data) {
                console.log(data);
                alert(deleteName + " deleted.");
                location.reload();
            }
        })
    }
}