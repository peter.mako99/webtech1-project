$(document).ready(function (){
    $.getJSON("https://webtechcars.herokuapp.com/api/manufacturers", function(data){
        var table =$("#manufacturerTable");
        $.each(data, function(key,value){
            var row = $('<tr></tr>');
            var idCell = $('<td>'+value._id+'</td>');
            var nameCell = $('<td>'+value.name+'</td>');
            var countryCell = $('<td>'+value.country+'</td>');
            var foundedCell = $('<td>'+value.founded+'</td>');
            var updateCell = $('<td>'+"<button type='button' onclick='createDialog(this);' data-string='manufacturers' class='btn btn-default'><img src='img/update.png'  width='50px'></button>"+'</td>');
            var deleteCell = $('<td>'+"<button type='button' onclick='deleteRecord(this);' data-string='manufacturers' class='btn btn-default'><img src='img/delete.png'  width='50px'></button>"+'</td>');
            $(row).append(idCell);
            $(row).append(nameCell);
            $(row).append(countryCell);
            $(row).append(foundedCell);
            $(row).append(updateCell);
            $(row).append(deleteCell);
            $(table).append(row);
        })
    })
})