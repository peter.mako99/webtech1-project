function addCar(event){
    event.preventDefault();
let car = {
    name: document.getElementsByName("name")[0].value,
    consumption: document.getElementsByName("consumption")[0].value+" l/100km",
    color: document.getElementsByName("color")[0].value,
    manufacturer: document.getElementsByName("manufacturer")[0].value,
    avaiable: document.getElementsByName("avaiable")[0].value,
    year: document.getElementsByName("year")[0].value,
    horsepower: document.getElementsByName("horsepower")[0].value
}
const carJson = JSON.stringify(car);

$.ajax({
    type: 'POST',
    url: "https://webtechcars.herokuapp.com/api/cars",
    data: carJson,
    error: function(e) {
      console.log(e);
    },
    contentType: "application/json",
    success: function (data) {
      console.log(data);
      alert("Car successfully added!");
      location.reload();
    }
  });
}