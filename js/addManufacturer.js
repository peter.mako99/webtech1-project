function addManufacturer(event){
    event.preventDefault();
let manufacturer = {
    name: document.getElementsByName("name")[0].value,
    country: document.getElementsByName("country")[0].value,
    founded: document.getElementsByName("founded")[0].value,
}
const manufacturerJson = JSON.stringify(manufacturer);

$.ajax({
    type: 'POST',
    url: "https://webtechcars.herokuapp.com/api/manufacturers",
    data: manufacturerJson,
    error: function(e) {
      console.log(e);
    },
    contentType: "application/json",
    success: function() { 
      alert("Manufacturer successfully added!");
      location.reload();
    }
  });
}
